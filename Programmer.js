import {Employee} from "./Employee.js";

export class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    };

    get salary() {
        return this._salary * 3
    };
}

const programmerFirst = new Programmer("Vasya", 46, 1500, "ukrainian, english, polish");
const programmerSecond = new Programmer('Zhorick', 21, 1000, 'ukrainian, moldavian');
const programmerThird = new Programmer('Ahmet', 35, 1200, 'arabian');

console.log(programmerFirst);
console.log(programmerSecond);
console.log(programmerThird);

